import React, { useEffect, useState } from "react";
import { Input, Button } from "antd";

function Login() {
  const [count, setCount] = useState(0);
  return (
    <>
      <div>login~~~</div>
      <button onClick={() => setCount(count + 1)}>{count}</button>
      <Input value={count} />
      <Button type="primary">Primary Button</Button>
      <Button>Default Button</Button>
      <Button type="dashed">Dashed Button</Button>
      <br />
      <Button type="text">Text Button</Button>
      <Button type="link">Link Button</Button>
    </>
  );
}
export default Login;
